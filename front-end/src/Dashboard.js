export default function Dashboard() {
  const [filter, setFilter] = useState({
    username: "",
    email: "",
    experience: "",
    lvl: "",
  });

  const [player, setPlayer] = useState([]);

  const handleOnChange = (e) => {
    setFilter({ ...filter, ...{ [e.target.name]: e.target.value } });
  };

  const fetchPlayer = () => {
    axios({
      method: "GET",
      url: "http://localhost:4000/api/v1/players",
    })
      .then((result) => {
        setFilter(result.data.data);
        setPlayer(result.data.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const handleFilter = () => {
    axios
      .get("http://localhost:4000/api/v1/players", { params: filter })
      .then((result) => {
        setPlayer(result.data.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    fetchPlayer();
  }, []);
  return {
    
    ( <h1>Yoweis' Dashboard </h1>;
  <div className="container">
    <div className="row">
        <div className="col form-group">
            <div className="form-group">
                <label htmlFor="username">Username</label>
                <input type="email" class="form-control" id="username" placeholder="username">
            </div>
        </div>
        <div className="col">
        <div className="col form-group">
                <label htmlFor="password">Email</label>
                <input type="email" class="form-control" id="email" placeholder="email@player.com">
            </div>
        </div>
        <div className="col">
        <div className="col form-group">
                <label htmlFor="password">Experience</label>
                <input type="number" class="form-control" id="experience" placeholder="10000">
            </div>
        </div>
        <div className="col">
        <div className="col form-group">
                <label htmlFor="username">Level</label>
                <input type="level" class="form-control" id="level" placeholder="250">
            </div>
        </div>
        <div className="col form-group">
            <button type="button" class="btn btn-light">Search</button>
        </div>
    </div>
  </div>

  <table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">First</th>
      <th scope="col">Last</th>
      <th scope="col">Handle</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Mark</td>
      <td>Otto</td>
      <td>@mdo</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Jacob</td>
      <td>Thornton</td>
      <td>@fat</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
    </tr>
  </tbody>
</table>

<table class="table">
  <thead class="thead-light">
    <tr>
      <th scope="col">#</th>
      <th scope="col">First</th>
      <th scope="col">Last</th>
      <th scope="col">Handle</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Mark</td>
      <td>Otto</td>
      <td>@mdo</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Jacob</td>
      <td>Thornton</td>
      <td>@fat</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
    </tr>
  </tbody>
</table>
  )
}
